**Gobierno Web de Pacífico Seguros**

Este conjunto de documentos refleja las políticas de gobierno web de Pacífico Seguros.


*Es un documento en constante actualización. Cualquier mejor o sugerencia por favor presentarla como pull request.*

---

## Elementos Principales

Estos son los elementos del Gobierno Web en Pacífico

0. **Introducción** - introducción al gobierno de la computación en la nube
1. **Audiencia** - público al que está dirigida la información
2. **Metas Corporativas** - metas corporativas de la computación en la nube
3. **Principios** - principios generales a seguir en el gobierno de la computación en la nube
4. **Conceptos, terminología, y estándares** - definiciones y conceptos para tener un entendimiento compartido de la computación en la nube
5. **Implicaciones estratégicas de la Computación en la Nube** - consideraciones empresariales sobre la computación en la nube
6. **Roles y Responsabilidades** - participantes en el gobierno y sus responsabilidades
7. **Procesos de gobierno de la nube** - procesos generales del gobierno de la computación en la nube, según TOGAF
8. **Políticas de Gobernabilidad** - políticas definidas que deben seguirse en todo proceso de computación en la nube
9. **Modelo de Aprobación para autorizaciones de implementación en Nube** - aspectos prioritarios a considerar en toda implementación de computación en la nube
10. **Catálogo de Componentes Estándares** - catálogo de componentes cloud cuyo uso se recomienda, con casos de uso para cada uno.
11. **Nomenclatura de Recursos** - estándar de nombre de componentes a seguir en cada implemnetación
12. **Lineamientos para despliegues en la nube** - lineamientos técnicos que deben observarse en cada implementación
14. **Catálogo de Aplicaciones en la Nube** - catálogo de aplicaciones implementadas que usan computación en la nube. Bajo actualización constante

---

## Proceso de actualización

Este documento está bajo control de Arquitectura de Innovación. Para proponer un cambio:

1. Generar un clone del repositorio git en su equipo
2. Realizar en el repositorio clonado los cambios requeridos
3. Presentar los cambios sugeridos como un **pull request**.
4. El **pull request** será evaluado por el responsable del repositorio y aprobado (previa coordinación de ser necesario) para su incorporación al documento final.
